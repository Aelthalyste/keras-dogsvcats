import numpy as np
import keras
import keras.layers
from keras.models import Model
from keras.layers import Input, Dense
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import plot_model
from keras.utils import to_categorical
from keras.models import save_model
from keras.models import load_model

def change_data_to_sigmoid_valid(data):
	new_data = np.zeros(shape = (len(data),1))
	for i in range(len(data)):
		if data[i][0] == 1:
			new_data[i] = 1
	return new_data

def get_data():
	return np.load("train_data.npy")

def create_model():
	model = Sequential()

	model.add(Conv2D(16,3,activation = "relu",input_shape = (50,50,1)))
	model.add(MaxPooling2D(2))

	model.add(Conv2D(25,3,activation = "relu",input_shape = (50,50,1)))
	model.add(MaxPooling2D(2))

	model.add(Conv2D(16,3,activation = "relu",input_shape = (50,50,1)))
	model.add(MaxPooling2D(2))

	model.add(Conv2D(25,3,activation = "relu",input_shape = (50,50,1)))
	model.add(MaxPooling2D(2))

	model.add(Conv2D(25,3,activation = "relu",input_shape = (50,50,1)))
	model.add(MaxPooling2D(2))

	

	model.add(keras.layers.Flatten())
	model.add(Dense(32))
	model.add(keras.layers.Activation("relu"))
	model.add(keras.layers.Dropout(0.7))
	model.add(Dense(2))
	model.add(keras.layers.Activation("softmax"))
	
	return model

def initialize_test_and_train_data(data,n=500):
	train = data[:-500]
	test = data[-500:]
	X = np.array([i[0] for i in train]).reshape(-1,50,50,1)
	y = [i[1] for i in train]
	y = change_data_to_sigmoid_valid(y)
	y = to_categorical(y)

	X_test = np.array([i[0] for i in test]).reshape(-1,50,50,1)
	y_test = [i[1] for i in test]
	y_test = change_data_to_sigmoid_valid(y_test)
	y_test = to_categorical(y_test)

	return X,y,X_test,y_test


model = load_model("mymodel.h5")
full_data = get_data()
X,y,X_test,y_test = initialize_test_and_train_data(full_data)

#img = cv2.resize(cv2.imread("test/2149.jpg",cv2.IMREAD_GRAYSCALE),(50,50))
#pr = np.array([img])
#pr = pr.reshape(-1,50,50,1)
#pred = str(model.predict(pr))
#pred = pred.replace("[","")
#pred = pred.replace("]","")

#nlist = pred.split(" ")
#if(nlist[0]>nlist[1]):
#	print("DOG")
#else:
#	print("CAT")

model.compile(optimizer="adam",loss="categorical_crossentropy",metrics=["accuracy"])
model.fit(X,y, epochs=10,batch_size = 100,validation_data = (X_test,y_test))
model.save("mymodel.h5")

